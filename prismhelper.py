# Eric van de Paverd 2016
# LGPL software

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from time import sleep
import getpass


class PrismHelper(object):
    def __init__(self, username, password, school_id):
        self.username = username
        self.password = password
        self.create_device_page = "https://apps.edustar.vic.edu.au/prism/itam/default.aspx#/device/{}".format(school_id)
        self.school_devices_page = "https://apps.edustar.vic.edu.au/prism/itam/default.aspx#/school/{}".format(school_id)
        self.school_audit_trail_page = "https://apps.edustar.vic.edu.au/prism/itam/default.aspx#/history/{}".format(school_id)

        self.browser = webdriver.Firefox()
        self.browser.get("https://www.edustar.vic.edu.au/prism")

        self.username_field = self.browser.find_element_by_id("username")
        self.password_field = self.browser.find_element_by_id("password")
        self.submit_button = self.browser.find_element_by_id("SubmitCreds")
        
        self.username_field.send_keys(self.username)
        self.password_field.send_keys(self.password)
        self.submit_button.click()

        self.browser.get("https://apps.edustar.vic.edu.au/prism/itam/default.aspx#/home")

        self.browser.get(self.school_devices_page)


    def add_device(self):
        self.browser.get(self.create_device_page)


    def copy_device(self, template_device_url, num_times):
        for t in range(num_times):

            # go to view device page
            self.browser.get(template_device_url)

            # find and click copy button
            while True:
                try:
                    button_to_click = self.browser.find_element_by_css_selector("button.btn:nth-child(1)")
                    button_to_click.click()
                    sleep(1)
                    break
                except:
                    sleep(1)
                    pass

            # find and click save button
            while True:
                try:
                    button_to_click = self.browser.find_element_by_css_selector("button.btn:nth-child(2)")
                    button_to_click.click()
                    sleep(1)
                    break
                except:
                    sleep(1)
                    pass

            print "finished adding item number: {}\n{} items to go\n========================".format(t+1, (num_times - t - 1))

        self.browser.get(self.school_devices_page)


    def rollback_devices(self, num_times):
        self.browser.get(self.school_audit_trail_page)
        sleep(7)
        for t in range(num_times):
            rollback_link = self.browser.find_element_by_css_selector("tr.table-tr:nth-child(2) > td:nth-child(5) > div:nth-child(1) > span:nth-child(1)")
            rollback_link.click()

            try:
                WebDriverWait(self.browser, 3).until(EC.alert_is_present(),
                                               'Timed out waiting for ' +
                                               'confirmation popup to appear.')

                alert = self.browser.switch_to_alert()
                alert.accept()
                sleep(1)
                print "finished adding item number: {}\n{} items to go\n========================".format(t+1, (num_times - t - 1))
            except TimeoutException:
                print "no alert box detected :( Quit and try running me again :("

        print "Done rolling back devices."
        self.browser.get(self.school_devices_page)

if __name__ == '__main__':

    #school id
    school_id = raw_input("Four digit school number: ")
    username = raw_input("User ID: ")
    password = getpass.getpass()

    prism_helper = PrismHelper(username=username, password=password, school_id=school_id)

    while True:
        action = raw_input("[A]dd device, [C]opy, [R]ollback, [H]elp, [Q]uit: ")

        # copy
        if action.lower() == 'c':
            template_device_url = raw_input("Template device URL (a to abort): ")
            if template_device_url.lower() == 'a':
                pass
            else:
                times_to_copy = raw_input("Number of times to copy (a to abort): ")
                if times_to_copy.lower() == 'a':
                    pass
                else:
                    prism_helper.copy_device(template_device_url, num_times=int(times_to_copy))

        # rollback
        elif action.lower() == 'r':
            times_to_rollback = raw_input("Number of times to rollback (a to abort): ")
            if times_to_rollback.lower() == 'a':
                pass
            else:
                prism_helper.rollback_devices(num_times=int(times_to_rollback))

        # help
        elif action.lower() == 'h':
            print """
    STEPS TO CREATE A DEVICE:
        1. Press "A" in this program to go to the add devices page for the current school
        2. Enter device details

    STEPS TO COPY A DEVICE:
        1. Click your school name under HOME > IT asset management > (SCHOOL NAME)
        2. Go to the "School Devices" tab
        3. Click "View" for the device category that has your "template" device
        4. Locate and copy the "View" link url FOR THE ACTUAL DEVICE YOU WANT TO COPY
        5. Press "C" in this program to make copies
        6. Paste the "View URL from step 3
        7. Enter the number of more copies you want

    STEPS TO ROLLBACK ENTRIES:
        1. Follow steps 1 & 2 for how to copy a device
        2. Click the "Bulk upload / audit trail" button
        3. Figure out how many entries you need to roll back
        4. Press "R" in this program to roll back entries
        5. Enter the number of to roll back

        PRO TIP:
        Since you can only roll back in reverse order, make sure 
        your devices are right AS YOU GO!!! 
        Otherwise, you may have to undo your latest work 
        to fix a muckup in your earlier work!!!

            """

        # quit
        elif action.lower() == 'q':
           print "Thanks for playing!!! Bye!!! :)"
           break
        
        # go to the "add device" page
        elif action.lower() == 'a':
            prism_helper.add_device()
        
        # not a valid action
        else:
            print "I don't know what you mean, try again!"
