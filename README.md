# README #

### ITAM assistance tool ###

* Copies a given count of a device you've added to ITAM
* Rolls back a given count

### Demo ###
[ITAM Helper demo](https://youtu.be/c0aec0fun0s) (non-edited, single take - don't expect professional quality)

### Benefits ###
* Allows users to add/remove multitudes of devices to ITAM with minimal effort
* No need to use the Excel spreadsheet (and deal with corruption, failed imports, etc.)
* Reasonably fast (as fast as the web interface can go)
* Can be left running in the background
* Cross platform (OSX, Windows, Linux... )

### Drawbacks ###
* Doesn't currently support the adding of serial numbers (I'll add this in the future, or feel free to add it yourself and share ;))
* Has to execute a lot of requests to the server
* Cannot do deletions or edits.  If you mess something up near the start of your data input process, and you realise at the end, you will have to do one of:
    * manually edit the records you stuffed up one by one :-(
    * use the spreadsheet to fix it :-(
    * roll back all your hard work and do it again - this is probably the easiest option if done the right way... see the tips below
### Usage / workflow ###
* Run script which will open a browser window to ITAM

##### Adding devices #####
* Enter a device using the browser and copy its 'View' link
* Give the script the URL and how many copies of that device to make

##### Rolling back entries #####
* Give the script the number of rollbacks to do

### Tips ###
1. It is a good idea to add all of your device types, CHECK THEM, and THEN start copying them.  Checking after having entered all of your device types makes you less prone to multiplying your mistakes.  Saving all of the copying for the end means that if you do have to roll back, you don't roll back any of your valuable "template" devices to undo your boo-boo, and it's then a single edit to your template device to fix your problem, and redo the copies.  

2. Remember the Firefox window doesn't need to maintain focus to keep doing the work (though don't open another tab/window within that Firefox session, because another focused tab in the same firefox session will mess it up).  You CAN open another instance of Firefox and browse with that, or use Chrome / MS Word, notepad.exe or any other program while the script / Firefox whirs away in the background.  

### Installation ###

You will need Python 2.7.xxx (ie. not version 3.x.xxx).  

#### Windows ####

1. Download and install python from the official site.  Make sure when you install it and get to the feature options, that you scroll down and include the option of installing python.exe on the system PATH.

2. You will then need to install the selenium library by going to powershell and entering (all one line):
`pip install selenium --proxy http://username:password@proxy.server.address.or.ip:portNumber`

3. Make sure firefox is installed on your computer (the script / selenium will use this to do the work)

4. Download / unzip this repository somewhere handy

5. Open PowerShell and navigate to the directory that has the itamhelper.py script

6. Enter into powershell (all one line):
`python prismhelper.py`

#### OSX ####

1. Make sure Firefox is installed on your computer (the script / selenium will use this to do the work)

2. Install the selenium library by going to terminal and entering (all one line):
`pip install selenium --proxy http://username:password@proxy.server.address.or.ip:portNumber`

3. Download / unzip this repository somewhere handy

4. Enter into terminal (all one line):
`python prismhelper.py`

### Disclaimer ###

PLEASE PLEASE PLEASE CHECK and DOUBLE CHECK your work BEFORE doing the copying.  This script will put a bit of load on the server, and with enough techs doing last-minute ITAM... well, let me just say that we all know how well the old timesheet system copes with everyone doing their timesheets at the same time, and you probably have more device entries than timesheet entries...  you have been warned.

I take no responsibility for how you use this software, and you take all responsibility.  If you don't accept this, you must not use this software.  

The software is a bit hacky, but seems to work reliably.  Feel free to add bug reports / feature requests. 

### Who do I talk to? ###

* st00398 - look me up on edumail